FROM python:3.9-alpine

ADD ./requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir -r /app/requirements.txt 

ADD ./ /app

WORKDIR /app
RUN chmod +x /app/es_update.py
