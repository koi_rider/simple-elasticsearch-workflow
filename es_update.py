import requests
import re
import os
import sys


def read_file(file_name):
    file = open(file_name, "r")
    return file.read().strip()


def get_es_connection(server_name):
    # We look inside "server_name" directory and look for .server file there
    connection_file = os.path.join(server_name, ".server")
    if os.path.isfile(connection_file):
        return read_file(connection_file)
    return None


def post_data(url, data):
    headers = {
        'Content-Type': 'application/json'
    }
    print(f"Attempting to put {len(data)} bytes of data to {url}")

    # ToDo: add proper error handling
    response = requests.put(url, headers=headers, data=data)
    # print(url)

    # print(f"Got response: {response.status_code}")
    # print(f"Response test: {response.text}")
    if response.status_code != 200:
        print(f"Response test: {response.text}")
    # else:
    #     return None


def main():

    if (len(sys.argv) == 2 and sys.argv[1].lower() in ("-h", "--help", "/?")):
        print('Bootstrap or update ElasticSearch configuration')
        print('Usage:')
        print('  python es_update.py [server name]')
        print('    if no server name is provided, will bootstrap / update all the servers in the current dir')
        return()

    path = '.'
    if (len(sys.argv) == 2):
        root_files = [sys.argv[1]]
    else:
        root_files = os.listdir(path)

    for server_name in root_files:
        if os.path.isdir(server_name) and not re.match("^\.", server_name):
            # We've found a non-dotted dir, we assume it's a server server_name
            # print("It's a non-dotted dir!")
            es_base_url = get_es_connection(server_name)
            if es_base_url != None and es_base_url != "":
                for obj_type in sorted(os.listdir(os.path.join(path, server_name))):
                    obj_base = os.path.join(path, server_name, obj_type)
                    print(obj_type)
                    if os.path.isdir(obj_base) and os.path.isfile(os.path.join(obj_base, ".base")):
                        api_path = read_file(os.path.join(obj_base, ".base"))
                        print(api_path)
                        for file_name in sorted(os.listdir(obj_base)):
                            file_full_name = os.path.join(obj_base, file_name)
                            if os.path.isfile(file_full_name) and re.match("^[^\.].+\.json$", file_name):
                                post_data(
                                    f"{es_base_url}{api_path}{os.path.splitext(file_name)[0]}", read_file(file_full_name))


if __name__ == "__main__":
    main()
